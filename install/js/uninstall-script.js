var steps = [
    {
        name: 'intro',
        title: 'Welcome',
        text: 'Uninstall',
        callback: undefined
    },
    {
        name: 'db-confirm',
        title: 'Database Information',
        text: 'Confirm the information below',
        callback: undefined
    },
    {
        name: 'rich-text-conversion',
        title: 'HTML Ticket / Replies Conversion',
        text: 'HTML → Plaintext Ticket Conversion',
        showBack: false,
        showNext: false,
        callback: checkForRickTextContent
    },
    {
        name: 'uninstall',
        title: 'Uninstall',
        text: 'Uninstalling...',
        showBack: false,
        showNext: false,
        callback: uninstall
    },
    {
        name: 'complete',
        title: 'Finished',
        text: 'Uninstall Process Complete',
        showBack: false,
        callback: undefined
    }
];

$(document).ready(function() {
    var currentStep = 0;

    $('#next-button').click(function() {
        goToStep(++currentStep);
    });

    $('#back-button').click(function() {
        goToStep(--currentStep);
    });
});

function goToStep(step) {
    $('[data-step]').hide();
    $('[data-step="' + steps[step].name + '"]').show();

    if (step === 0) {
        $('#tools-button').show();
        $('#back-button').css('visibility', 'hidden');
    } else {
        $('#tools-button').hide();
        $('#back-button').css('visibility', 'visible');
    }

    if (step === steps.length - 1) {
        $('#next-button').css('visibility', 'hidden');
    } else {
        $('#next-button').css('visibility', 'visible');
    }

    // Back/Next button overrides
    if (steps[step].showBack !== undefined && !steps[step].showBack) {
        $('#back-button').css('visibility', 'hidden');
    }
    if (steps[step].showNext !== undefined && !steps[step].showNext) {
        $('#next-button').css('visibility', 'hidden');
    }

    $('#header-text').text(steps[step].text);

    if (steps[step].callback !== undefined) {
        steps[step].callback();
    }
}

//region HTML to Plaintext
function checkForRickTextContent() {
    var heskPath = $('p#hesk-path').text();

    $.ajax({
        url: heskPath + 'install/ajax/check-rich-text-content.php',
        method: 'GET',
        success: function(data) {
            $('#convert-checking').hide();
            if (data.needsMigrating) {
                $('#convert-found').show();
                $('#convert-found').html($('#convert-found').html().replace('{{number}}', data.numberOfRecords));
            } else {
                $('#convert-unnecessary').show();
                $('#next-button').css('visibility', 'visible');
            }

            console.log(data);
        }
    });
}

var g_completedRecords = 0;
function convertBatchOfTickets() {
    var heskPath = $('p#hesk-path').text();

    $('#convert-found').hide();
    $('#convert-running').show();

    $.ajax({
        url: heskPath + 'install/ajax/convert-rich-text-content.php',
        method: 'GET',
        success: function(data) {
            g_completedRecords += data.processedRecords;

            let $conversionStatus = $('#conversion-status');
            $conversionStatus.html($conversionStatus.html() + '<br>Processed ' + g_completedRecords + ' records.');

            if (data.finished) {
                $('#convert-finished').show();
                $('#convert-running').hide();
                $('#next-button').css('visibility', 'visible');
            } else {
                convertBatchOfTickets();
            }
        },
        error: function(response) {
            $('#convert-error').show();
            $('#convert-running').hide();
        }
    });
}
//endregion

//region Uninstaller
function uninstall() {
    var startingChunk = 1;

    $('[data-step="uninstall"] > #spinner').hide();
    $('[data-step="uninstall"] > .chunks').show();

    // Recursive call that will increment by 1 each time
    executeChunk(startingChunk, startingChunk, 4, 'up');
}

function executeChunk(startingMigrationNumber, migrationNumber, latestMigrationNumber, direction) {
    var heskPath = $('p#hesk-path').text();

    $.ajax({
        url: heskPath + 'install/ajax/uninstall-everything.php?chunk=' + migrationNumber,
        method: 'GET',
        success: function(data) {
            console.log('migrationNumber: ' + migrationNumber);
            console.log('latestMigrationNumber: ' + latestMigrationNumber);
            console.info('---');
            if (migrationNumber === latestMigrationNumber) {
                updateChunkStatus(startingMigrationNumber, migrationNumber, false, true);
                console.log('%c Success! ', 'color: white; background-color: green; font-size: 2em');
            } else {
                updateChunkStatus(startingMigrationNumber, migrationNumber, false, false);
                var newMigrationNumber = direction === 'up' ? migrationNumber + 1 : migrationNumber - 1;
                executeChunk(startingMigrationNumber, newMigrationNumber, latestMigrationNumber, direction);
            }
        },
        error: function(response) {
            try {
                message = JSON.parse(response);
            } catch (e) {
                message = response.responseText;
            }
            $errorBlock = $('#error-block');
            $errorBlock.html($errorBlock.html() + "<br><br>An error occurred! (Error Code: " + migrationNumber + ")<br>" + message).show();

            updateChunkStatus(startingMigrationNumber, migrationNumber, true, false);

            console.error(message);
        }
    })
}

function updateChunkStatus(startingMigrationNumber, migrationNumber, isError, isFinished) {
    var $chunkStatus = $('.chunk-' + migrationNumber).find('.status').find('i');

    if (isError === true) {
        $chunkStatus.removeClass('fa-circle').addClass('fa-times-circle');
    } else {
        $chunkStatus.removeClass('fa-circle').addClass('fa-check-circle');
    }

    if (isFinished && !isError) {
        goToStep(steps.length - 1);
    }
}
//endregion
