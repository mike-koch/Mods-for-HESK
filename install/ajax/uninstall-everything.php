<?php
define('IN_SCRIPT', 1);
define('HESK_PATH', '../../');
require(HESK_PATH . 'hesk_settings.inc.php');
require(HESK_PATH . 'inc/common.inc.php');
require(HESK_PATH . 'inc/admin_functions.inc.php');
hesk_load_database_functions();

hesk_dbConnect();

$chunk = intval($_GET['chunk']);
// Chunk 1 - Extraneous columns MFH added
if ($chunk === 1) {
    safeDropColumn('attachments', 'note_id');
    safeDropColumn('attachments', 'download_count');

    safeDropColumn('categories', 'manager');
    safeDropColumn('categories', 'color');
    safeDropColumn('categories', 'background_color');
    safeDropColumn('categories', 'usage');
    safeDropColumn('categories', 'mfh_parent_id');
    safeDropColumn('categories', 'foreground_color');
    safeDropColumn('categories', 'display_border_outline');
    safeDropColumn('categories', 'mfh_description');
    safeDropColumn('categories', 'mfh_category_group_id');

    safeDropColumn('kb_attachments', 'download_count');

    safeDropColumn('notes', 'edit_date');
    safeDropColumn('notes', 'number_of_edits');

    safeDropColumn('replies', 'html');

    safeDropColumn('service_messages', 'icon');
    safeDropColumn('service_messages', 'mfh_language');

    safeDropColumn('tickets', 'parent');
    safeDropColumn('tickets', 'latitude');
    safeDropColumn('tickets', 'longitude');
    safeDropColumn('tickets', 'html');
    safeDropColumn('tickets', 'user_agent');
    safeDropColumn('tickets', 'screen_resolution_width');
    safeDropColumn('tickets', 'screen_resolution_height');
    safeDropColumn('tickets', 'due_date');
    safeDropColumn('tickets', 'overdue_email_sent');

    safeDropColumn('users', 'notify_note_unassigned');
    safeDropColumn('users', 'default_calendar_view');
    safeDropColumn('users', 'notify_overdue_unassigned');
    safeDropColumn('users', 'active');
    safeDropColumn('users', 'permission_template');
}

// Chunk 2 - Extraneous tables MFH added
if ($chunk === 2) {
    safeDropTable('audit_trail');
    safeDropTable('audit_trail_to_replacement_values');
    safeDropTable('calendar_event');
    safeDropTable('calendar_event_reminder');
    safeDropTable('custom_nav_element');
    safeDropTable('custom_nav_element_to_text');
    safeDropTable('logging');
    safeDropTable('mfh_calendar_business_hours');
    safeDropTable('mfh_category_groups');
    safeDropTable('mfh_category_groups_i18n');
    safeDropTable('mfh_service_message_to_location');
    safeDropTable('pending_verification_emails');
    safeDropTable('permission_templates');
    safeDropTable('quick_help_sections');
    safeDropTable('settings');
    safeDropTable('stage_tickets');
    safeDropTable('temp_attachment');
    safeDropTable('text_to_status_xref');
    safeDropTable('user_api_tokens');
    safeDropTable('verified_emails');
}

// Chunk 3 - Status table
if ($chunk === 3) {
    if (isset($_GET['simulate'])) {
        sleep(2);
    } else {
        $new = getStatusId('IsNewTicketStatus', 0);
        $customerReply = getStatusId('IsCustomerReplyStatus', 1);
        $defaultStaffReply = getStatusId('IsDefaultStaffReplyStatus', 2);
        // Closed Statuses
        $closedStatuses = array();
        $closedRs = executeQuery("SELECT `ID` FROM `" . hesk_dbEscape($hesk_settings['db_pfix']) . "statuses` WHERE `IsClosed` = 1");
        while ($row = hesk_dbFetchAssoc($closedRs)) {
            array_push($closedStatuses, $row['ID']);
        }

        executeQuery("UPDATE `" . hesk_dbEscape($hesk_settings['db_pfix']) . "tickets` SET `status` = 0, `lastchange`=`lastchange` WHERE `status` = " . intval($new));
        executeQuery("UPDATE `" . hesk_dbEscape($hesk_settings['db_pfix']) . "tickets` SET `status` = 1, `lastchange`=`lastchange` WHERE `status` = " . intval($customerReply));
        executeQuery("UPDATE `" . hesk_dbEscape($hesk_settings['db_pfix']) . "tickets` SET `status` = 2, `lastchange`=`lastchange` WHERE `status` = " . intval($defaultStaffReply));

        $closedStatusString = implode(',', $closedStatuses);
        executeQuery("UPDATE `" . hesk_dbEscape($hesk_settings['db_pfix']) . "tickets` SET `status` = 3, `lastchange`=`lastchange` WHERE `status` IN ({$closedStatusString})");
        executeQuery("UPDATE `" . hesk_dbEscape($hesk_settings['db_pfix']) . "tickets` SET `status` = 4, `lastchange`=`lastchange` WHERE `status` NOT IN ({$new}, {$customerReply}, {$defaultStaffReply}, {$closedStatusString})");

        safeDropTable('statuses');
    }
}

// Chunk 4 - Files / folders
if ($chunk === 4) {
    if (isset($_GET['simulate'])) {
        sleep(2);
    } else {
        // Folders
        hesk_rrmdir(HESK_PATH.'api');
        hesk_rrmdir(HESK_PATH.'cron');
        hesk_rrmdir(HESK_PATH.'custom');
        hesk_rrmdir(HESK_PATH.'fonts');
        hesk_rrmdir(HESK_PATH.'img/bootstrap-colorpicker');
        hesk_rrmdir(HESK_PATH.'img/leaflet');
        hesk_rrmdir(HESK_PATH.'inc/custom');
        hesk_rrmdir(HESK_PATH.'internal-api');
        hesk_rrmdir(HESK_PATH.'js/calendar');
        hesk_rrmdir(HESK_PATH.'js/images');
        hesk_rrmdir(HESK_PATH.'locales');

        // Files
        hesk_unlink(HESK_PATH.'apidoc.json');
        hesk_unlink(HESK_PATH.'calendar.php');
        hesk_unlink(HESK_PATH.'verifyemail.php');
        hesk_unlink(HESK_PATH.'admin/api_settings.php');
        hesk_unlink(HESK_PATH.'admin/calendar.php');
        hesk_unlink(HESK_PATH.'admin/manage_email_templates.php');
        hesk_unlink(HESK_PATH.'admin/manage_category_groups.php');
        hesk_unlink(HESK_PATH.'admin/manage_custom_nav_elements.php');
        hesk_unlink(HESK_PATH.'admin/manage_permission_groups.php');
        hesk_unlink(HESK_PATH.'admin/manage_statuses.php');
        hesk_unlink(HESK_PATH.'admin/view_message_log.php');
        hesk_unlink(HESK_PATH.'css/AdminLTE.min.css');
        hesk_unlink(HESK_PATH.'css/bootstrap.css');
        hesk_unlink(HESK_PATH.'css/bootstrap-clockpicker.min.css');
        hesk_unlink(HESK_PATH.'css/bootstrap-colorpicker.min.css');
        hesk_unlink(HESK_PATH.'css/bootstrap-iconpicker.min.css');
        hesk_unlink(HESK_PATH.'css/bootstrap-select.min.css');
        hesk_unlink(HESK_PATH.'css/bootstrap-theme.css');
        hesk_unlink(HESK_PATH.'css/colors.css');
        hesk_unlink(HESK_PATH.'css/datepicker.css');
        hesk_unlink(HESK_PATH.'css/displays.css');
        hesk_unlink(HESK_PATH.'css/dropzone.min.css');
        hesk_unlink(HESK_PATH.'css/dropzone-basic.min.css');
        hesk_unlink(HESK_PATH.'css/font-awesome.min.css');
        hesk_unlink(HESK_PATH.'css/fullcalendar.min.css');
        hesk_unlink(HESK_PATH.'css/hesk_newStyle.css');
        hesk_unlink(HESK_PATH.'css/leaflet.css');
        hesk_unlink(HESK_PATH.'css/magnific-popup.css');
        hesk_unlink(HESK_PATH.'css/mods-for-hesk.css');
        hesk_unlink(HESK_PATH.'css/mods-for-hesk-new.css');
        hesk_unlink(HESK_PATH.'css/octicons.css');
        hesk_unlink(HESK_PATH.'css/octicons.eot');
        hesk_unlink(HESK_PATH.'css/octicons.svg');
        hesk_unlink(HESK_PATH.'css/positions.css');
        hesk_unlink(HESK_PATH.'css/toastr.min.css');
        hesk_unlink(HESK_PATH.'inc/category_groups.inc.php');
        hesk_unlink(HESK_PATH.'inc/headerAdmin.inc.php');
        hesk_unlink(HESK_PATH.'inc/htmLawed.inc.php');
        hesk_unlink(HESK_PATH.'inc/status_functions.inc.php');
        hesk_unlink(HESK_PATH.'inc/view_attachment_functions.inc.php');
        hesk_unlink(HESK_PATH.'js/adminlte.min.js');
        hesk_unlink(HESK_PATH.'js/bootstrap.min.js');
        hesk_unlink(HESK_PATH.'js/bootstrap-clockpicker.min.js');
        hesk_unlink(HESK_PATH.'js/bootstrap-colorpicker.min.js');
        hesk_unlink(HESK_PATH.'js/bootstrap-datepicker.js');
        hesk_unlink(HESK_PATH.'js/bootstrap-iconpicker.js');
        hesk_unlink(HESK_PATH.'js/bootstrap-select.js');
        hesk_unlink(HESK_PATH.'js/bootstrap-validator.min.js');
        hesk_unlink(HESK_PATH.'js/clipboard.min.js');
        hesk_unlink(HESK_PATH.'js/dropzone.min.js');
        hesk_unlink(HESK_PATH.'js/iconset-fontawesome-4.3.0.js');
        hesk_unlink(HESK_PATH.'js/iconset-octicon-2.1.2.js');
        hesk_unlink(HESK_PATH.'js/jquery.dirtyforms.min.js');
        hesk_unlink(HESK_PATH.'js/jquery.magnific-popup.min.js');
        hesk_unlink(HESK_PATH.'js/jquery.slimscroll.min.js');
        hesk_unlink(HESK_PATH.'js/jquery-1.10.2.min.js');
        hesk_unlink(HESK_PATH.'js/jquery-2.2.4.min.js');
        hesk_unlink(HESK_PATH.'js/jstree.min.js');
        hesk_unlink(HESK_PATH.'js/jstreegrid.js');
        hesk_unlink(HESK_PATH.'js/leaflet.js');
        hesk_unlink(HESK_PATH.'js/modsForHesk-javascript.js');
        hesk_unlink(HESK_PATH.'js/platform.js');
        hesk_unlink(HESK_PATH.'js/sprintf.min.js');
        hesk_unlink(HESK_PATH.'js/table-dragger.min.js');
        hesk_unlink(HESK_PATH.'js/toastr.min.js');
        hesk_unlink(HESK_PATH.'js/validation-scripts.js');
    }
}

output(array(
    'status' => 'complete',
    'chunk' => $chunk
));
return;


// Helper functions
function safeDropColumn($table, $column) {
    global $hesk_settings;

    if (isset($_GET['simulate'])) {
        usleep(100000);
        return;
    }

    $formatted_table_name = $hesk_settings['db_pfix'] . $table;
    $res = executeQuery("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '".hesk_dbEscape($formatted_table_name)."' AND table_schema = '".hesk_dbEscape($hesk_settings['db_name'])."' AND column_name = '".$column."' LIMIT 0, 1");

    if (hesk_dbNumRows($res) !== 0) {
        executeQuery("ALTER TABLE `" . hesk_dbEscape($formatted_table_name) . "` DROP COLUMN `" . $column . "`");
    }
}

function safeDropTable($table) {
    global $hesk_settings;

    if (isset($_GET['simulate'])) {
        usleep(100000);
        return;
    }

    $formatted_table_name = $hesk_settings['db_pfix'] . $table;
    $res = executeQuery("SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE table_name = '".hesk_dbEscape($formatted_table_name)."' AND table_schema = '".hesk_dbEscape($hesk_settings['db_name'])."' LIMIT 0, 1");

    if (hesk_dbNumRows($res) !== 0) {
        executeQuery("DROP TABLE `" . hesk_dbEscape($formatted_table_name) . "`");
    }
}

function getStatusId($statusType, $default) {
    global $hesk_settings;

    $newRs = executeQuery("SELECT `ID` FROM `" . hesk_dbEscape($hesk_settings['db_pfix']) . "statuses` WHERE `{$statusType}` = 1");
    if ($row = hesk_dbFetchAssoc($newRs)) {
        return $row['ID'];
    }

    return $default;
}

function executeQuery($sql) {
    global $hesk_last_query;
    global $hesk_db_link;
    if (function_exists('mysqli_connect')) {

        if (!$hesk_db_link && !hesk_dbConnect()) {
            return false;
        }

        $hesk_last_query = $sql;

        if ($res = @mysqli_query($hesk_db_link, $sql)) {
            return $res;
        } else {
            http_response_code(500);
            print "Could not execute query: $sql. MySQL said: " . mysqli_error($hesk_db_link);
            die();
        }
    } else {
        if (!$hesk_db_link && !hesk_dbConnect()) {
            return false;
        }

        $hesk_last_query = $sql;

        if ($res = @mysql_query($sql, $hesk_db_link)) {
            return $res;
        } else {
            http_response_code(500);
            print "Could not execute query: $sql. MySQL said: " . mysql_error();
            die();
        }
    }
}

function output($data, $response = 200, $header = "Content-Type: application/json") {
    http_response_code($response);
    header($header);
    print json_encode($data);
}
