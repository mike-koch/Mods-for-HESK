<?php
define('IN_SCRIPT', 1);
define('HESK_PATH', '../../');
require(HESK_PATH . 'hesk_settings.inc.php');
require(HESK_PATH . 'inc/common.inc.php');
hesk_load_database_functions();


hesk_dbConnect();

$format = "SELECT COUNT(1) AS `cnt` FROM `" . hesk_dbEscape($hesk_settings['db_pfix']) . "%s` WHERE `html` = '1'";

$res = hesk_dbQuery(sprintf($format, 'tickets') . " UNION " . sprintf($format, 'replies'));

$totalNumberOfRecords = 0;
while ($row = hesk_dbFetchAssoc($res)) {
    $totalNumberOfRecords += $row['cnt'];
}

output([
    'needsMigrating' => $totalNumberOfRecords > 0,
    'numberOfRecords' => $totalNumberOfRecords
]);

function output($data, $response = 200, $header = "Content-Type: application/json") {
    http_response_code($response);
    header($header);
    print json_encode($data);
}
