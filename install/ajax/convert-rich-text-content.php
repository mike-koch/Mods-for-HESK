<?php
define('IN_SCRIPT', 1);
define('HESK_PATH', '../../');
require(HESK_PATH . 'hesk_settings.inc.php');
require(HESK_PATH . 'inc/common.inc.php');
require(HESK_PATH . 'inc/html2text/html2text.php');
hesk_load_database_functions();


hesk_dbConnect();

if (isset($_GET['simulate'])) {
    output(array(
        'finished' => true,
        'processedRecords' => 4747
    ));
    return;
}

$processingLimit = 20;
$numberProcessed = 0;
// 1. Check for ticket messages to convert
$ticketRs = hesk_dbQuery("SELECT `id`, `message` FROM `" . hesk_dbEscape($hesk_settings['db_pfix']) . "tickets` WHERE `html` = '1' LIMIT {$processingLimit}");
while ($row = hesk_dbFetchAssoc($ticketRs)) {
    $newMessage = hesk_html_entity_decode($row['message']);
    $newMessage = convert_html_to_text($newMessage);
    $newMessage = fix_newlines($newMessage);

    hesk_dbQuery("UPDATE `" . hesk_dbEscape($hesk_settings['db_pfix']) . "tickets` SET `message` = '" . hesk_dbEscape($newMessage) . "', `html` = '0', `lastchange`=`lastchange` WHERE `id` = ".intval($row['id']));

    $numberProcessed++;
}

// 2. If we didn't process enough tickets, process some replies
if ($numberProcessed < $processingLimit) {
    $numberOfRepliesToProcess = $processingLimit - $numberProcessed;
    $repliesRs = hesk_dbQuery("SELECT `id`, `message` FROM `" . hesk_dbEscape($hesk_settings['db_pfix']) . "replies` WHERE `html` = '1' LIMIT {$numberOfRepliesToProcess}");
    while ($row = hesk_dbFetchAssoc($repliesRs)) {
        $newMessage = hesk_html_entity_decode($row['message']);
        $newMessage = convert_html_to_text($newMessage);
        $newMessage = fix_newlines($newMessage);

        hesk_dbQuery("UPDATE `" . hesk_dbEscape($hesk_settings['db_pfix']) . "replies` SET `message` = '" . hesk_dbEscape($newMessage) . "', `html` = '0' WHERE `id` = ".intval($row['id']));

        $numberProcessed++;
    }
}

output(array(
    'finished' => $numberProcessed === 0,
    'processedRecords' => $numberProcessed
));

function output($data, $response = 200, $header = "Content-Type: application/json") {
    http_response_code($response);
    header($header);
    print json_encode($data);
}
